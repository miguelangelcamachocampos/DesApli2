package utng.da2.macc;

import java.io.Serializable;

public class Calcula implements Serializable{
	private double b;
	private double h;
	private double result;
	
	public double getResult() {
		return b * h / 2;
	}

	public void setResult(double result) {
		this.result = result;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}

	public double getB() {
		return b;
	}

	public void setB(double b) {
		this.b = b;
	}

	public double getH() {
		return h;
	}

	public void setH(double h) {
		this.h = h;
	}
	
	
}
